//
//  WPListViewController.m
//  westpactest
//
//  Created by Johnny ddb on 2/07/2014.
//  Copyright (c) 2014 westpac. All rights reserved.
//

#import "WPListViewController.h"
#import "WPListTableViewCell.h"
#import "WPWebViewController.h"

#define kTableCellHeight      150.0f

static NSString *CellIdentifier = @"CellIdentifier";

@interface WPListViewController ()

@property(nonatomic, retain, readonly) NSString *cellNibName;
@property(nonatomic, retain) WPListTableViewCell *prototypeCell; // For dynamic height caculation
// A dictionary of offscreen cells that are used within the tableView:heightForRowAtIndexPath: method to
// handle the height calculations. These are never drawn onscreen. The dictionary is in the format:
//      { NSString *reuseIdentifier : UITableViewCell *offscreenCell, ... }
@property (strong, nonatomic) NSMutableDictionary *offscreenCells;

@property(nonatomic, retain) UITableView * tableView;
@property(nonatomic, retain) NSArray * jsonMutableArray;

@end

@implementation WPListViewController

#pragma mark - Custom Getter
- (NSString *)cellNibName{
    return @"WPListTableViewCell";
}

- (WPListTableViewCell *)prototypeCell{
    if (!_prototypeCell)
    {
        _prototypeCell = [self.tableView dequeueReusableCellWithIdentifier:@"WPListTableViewCell"];
    }
    return _prototypeCell;
}
/*
 - (NSMutableArray *)jsonMutableArray{
 if(!_jsonMutableArray){
 _jsonMutableArray = [[NSMutableArray alloc] init];
 }
 
 return _jsonMutableArray;
 }*/

- (void)dealloc {
    _tableView=nil;
    //[_jsonMutableArray release], _jsonMutableArray=nil;
    
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = APP_NAME;
    
    // Tableview setup
    self.tableView = [[[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height) style:UITableViewStylePlain] autorelease];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.tableView];
    
    // Add footer for no empty cell
    UIView *footer = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, _tableView.frame.size.width, 1)] autorelease];
    footer.backgroundColor = [UIColor clearColor];
    [_tableView setTableFooterView:footer];
    
    UIBarButtonItem *refreshBarButton = [[[UIBarButtonItem alloc] initWithTitle:@"Refresh" style:UIBarButtonItemStyleBordered target:self action:@selector(fetchDataFromURL)] autorelease];
    
    self.navigationItem.leftBarButtonItems = @[refreshBarButton];
    
    // Register custom cell class
    [self.tableView registerClass:[WPListTableViewCell class] forCellReuseIdentifier:self.cellNibName];
    
    [self fetchDataFromURL];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}

#pragma mark - Private
-(void)fetchDataFromURL{
    [self fetchDataFromURL:nil];
}

-(void)fetchDataFromURL:(id)sender
{
    //__block NSMutableArray *jsonMutableArray = self.jsonMutableArray;
    // fetch store data on a separate thread
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSURL * const url = [NSURL URLWithString:DATA_URL];
        NSData * const responseData = [NSData dataWithContentsOfURL:url];
        
        if(responseData){
            NSError *jsonError = nil;
            NSDictionary * const responseObject = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&jsonError];
            
            if (responseObject) {
                NSArray *responseArray = [[[NSArray alloc] initWithArray:[responseObject objectForKey:@"items"]] autorelease];
                
                self.jsonMutableArray = [responseArray sortedArrayUsingComparator:^NSComparisonResult(id firstDateString, id secondDateString) {
                    NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
                    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:SSZZZZ"];
                    NSDate *firstDate = [formatter dateFromString:[(NSDictionary *)firstDateString objectForKey:@"dateLine"]];
                    ;
                    NSDate *secondDate = [formatter dateFromString:[(NSDictionary *)secondDateString objectForKey:@"dateLine"]];
                    
                    return [secondDate compare:firstDate];
                }];
                
                NSLog(@"%@", self.jsonMutableArray);
                
                // refresh table with store data from network
                dispatch_async(dispatch_get_main_queue(),^{
                    [self.tableView reloadData];
                });
            }
        }
        else {
            // error msg
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Error loading data"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
    });
}

#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // This project has only one cell identifier, but if you are have more than one, this is the time
    // to figure out which reuse identifier should be used for the cell at this index path.
    NSString *reuseIdentifier = CellIdentifier;
    
    // Use the dictionary of offscreen cells to get a cell for the reuse identifier, creating a cell and storing
    // it in the dictionary if one hasn't already been added for the reuse identifier.
    // WARNING: Don't call the table view's dequeueReusableCellWithIdentifier: method here because this will result
    // in a memory leak as the cell is created but never returned from the tableView:cellForRowAtIndexPath: method!
    WPListTableViewCell *cell = [self.offscreenCells objectForKey:reuseIdentifier];
    if (!cell) {
        cell = [[WPListTableViewCell alloc] init];
        [self.offscreenCells setObject:cell forKey:reuseIdentifier];
    }
    
    // Configure the cell for this indexPath
    [self configureCell:cell forRowAtIndexPath:indexPath];
    
    // Make sure the constraints have been added to this cell, since it may have just been created from scratch
    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded];
    
    // The cell's width must be set to the same size it will end up at once it is in the table view.
    // This is important so that we'll get the correct height for different table view widths, since our cell's
    // height depends on its width due to the multi-line UILabel word wrapping. Don't need to do this above in
    // -[tableView:cellForRowAtIndexPath:] because it happens automatically when the cell is used in the table view.
    cell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.bounds), CGRectGetHeight(cell.bounds));
    // NOTE: if you are displaying a section index (e.g. alphabet along the right side of the table view), or
    // if you are using a grouped table view style where cells have insets to the edges of the table view,
    // you'll need to adjust the cell.bounds.size.width to be smaller than the full width of the table view we just
    // set it to above. See http://stackoverflow.com/questions/3647242 for discussion on the section index width.
    
    // Do the layout pass on the cell, which will calculate the frames for all the views based on the constraints
    // (Note that the preferredMaxLayoutWidth is set on multi-line UILabels inside the -[layoutSubviews] method
    // in the UITableViewCell subclass
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    
    // Get the actual height required for the cell
    CGFloat height = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    
    // Add an extra point to the height to account for the cell separator, which is added between the bottom
    // of the cell's contentView and the bottom of the table view cell.
    height += 1;
    
    return height;
    //return kTableCellHeight;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString * tinyUrl = [[[NSString alloc] initWithFormat:@"%@", [[self.jsonMutableArray objectAtIndex:indexPath.row] objectForKey:@"tinyUrl"]] autorelease];
    
    WPWebViewController * detailedViewController = [[[WPWebViewController alloc] initWithUrls:[NSURL URLWithString:tinyUrl]] autorelease];
	
	[self.navigationController pushViewController:detailedViewController animated:YES];
}

#pragma mark - UITableView Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.jsonMutableArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WPListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellNibName forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[[WPListTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:self.nibName] autorelease];
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [self configureCell:cell forRowAtIndexPath:indexPath];
    
    // Make sure the constraints have been added to this cell, since it may have just been created from scratch
    [cell setNeedsUpdateConstraints];
    //[cell updateConstraintsIfNeeded];
    [cell layoutIfNeeded];
    
    return cell;
}

#pragma mark - table cell configure method
- (void)configureCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"%@ indexPath Section:%d", NSStringFromSelector(_cmd), indexPath.section);
    if ([cell isKindOfClass:[WPListTableViewCell class]])
    {
        WPListTableViewCell *tableCell= (WPListTableViewCell *)cell;
        
        // Data parsing
        NSString * headLine = [[[NSString alloc] initWithFormat:@"%@", [[self.jsonMutableArray  objectAtIndex:indexPath.row] objectForKey:@"headLine"]] autorelease];
        NSString * slugLine = [[[NSString alloc] initWithFormat:@"%@", [[self.jsonMutableArray  objectAtIndex:indexPath.row] objectForKey:@"slugLine"]] autorelease];
        NSString * dateLine = [[[NSString alloc] initWithFormat:@"%@", [[self.jsonMutableArray  objectAtIndex:indexPath.row] objectForKey:@"dateLine"]] autorelease];
        NSString * thumbnailURL = [[[NSString alloc] initWithFormat:@"%@", [[self.jsonMutableArray objectAtIndex:indexPath.row] objectForKey:@"thumbnailImageHref"]]autorelease];
        NSString * identifier = [[[NSString alloc] initWithFormat:@"id_%@", [[self.jsonMutableArray objectAtIndex:indexPath.row] objectForKey:@"identifier"]]autorelease];
        
        [tableCell.headLine setText:headLine];
        [tableCell.slugLine setText:slugLine];
        [tableCell updateDate:dateLine];
        
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            // Remote Image Update
            if(thumbnailURL && (NSNull *)thumbnailURL != [NSNull null]){
                [tableCell updateImageURL:thumbnailURL WithIdentifier:identifier];
            }
        });
    }
}

//support orientation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

@end
