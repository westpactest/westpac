//
//  UIView+Autolayout.m
//  westpactest
//
//  Created by Johnny ddb on 2/07/2014.
//  Copyright (c) 2014 westpac. All rights reserved.
//

#import "UIView+Autolayout.h"

@implementation UIView (Autolayout)

+ (id)autolayoutView
{
    UIView *view = [[[self alloc] init] autorelease];
    view.translatesAutoresizingMaskIntoConstraints = NO;
    return view;
}

@end
