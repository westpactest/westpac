//
//  UIColor+Text.m
//  westpactest
//
//  Created by Johnny ddb on 2/07/2014.
//  Copyright (c) 2014 westpac. All rights reserved.
//

#import "UIColor+Text.h"

@implementation UIColor (Text)

+ (UIColor *)WPLabelTextColor{
    return [UIColor colorWithRed:.285 green:.376 blue:.541 alpha:1];
}

@end
