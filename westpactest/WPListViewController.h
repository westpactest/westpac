//
//  WPListViewController.h
//  westpactest
//
//  Created by Johnny ddb on 2/07/2014.
//  Copyright (c) 2014 westpac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WPListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@end
