//
//  WPConstants.h
//  westpactest
//
//  Created by Johnny ddb on 2/07/2014.
//  Copyright (c) 2014 westpac. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const APP_NAME;
extern NSString * const DATA_URL;

extern float const FONT_SIZE_SMALLER;
extern float const FONT_SIZE_SMALL;
extern float const FONT_SIZE_MEDIUM;
extern float const FONT_SIZE_LARGER;
