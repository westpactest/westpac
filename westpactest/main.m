//
//  main.m
//  westpactest
//
//  Created by Johnny ddb on 2/07/2014.
//  Copyright (c) 2014 westpac. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([WPAppDelegate class]));
    }
}
