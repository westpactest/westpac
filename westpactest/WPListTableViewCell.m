//
//  WPListTableViewCell.m
//  westpactest
//
//  Created by Johnny ddb on 2/07/2014.
//  Copyright (c) 2014 westpac. All rights reserved.
//

#import "WPListTableViewCell.h"
#import "UIView+Autolayout.h"
#import "UIColor+Text.h"

#define kLabelVerticalInsets        10.0f
#define kLabelHorizontalInsets      15.0f

#define dispatch_main_sync_safe(block)\
if ([NSThread isMainThread]) {\
block();\
}\
else {\
dispatch_sync(dispatch_get_main_queue(), block);\
}

@interface WPListTableViewCell()

@property(nonatomic, retain) NSCache * imageCache;
@property (nonatomic, assign) BOOL didSetupConstraints;

@end

@implementation WPListTableViewCell

- (void)dealloc {
    [_headLine release], _headLine=nil;
    [_slugLine release], _slugLine=nil;
    [_thumbnailImageView release], _thumbnailImageView=nil;
    [_dateLine release], _dateLine=nil;
    _imageCache=nil;
    
    [super dealloc];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        //headLine
        _headLine = [[UILabel autolayoutView] retain];
        [_headLine setBackgroundColor:[UIColor clearColor]];
        [_headLine setNumberOfLines:0];
        [_headLine setLineBreakMode:NSLineBreakByWordWrapping];
        [_headLine setTextColor:[UIColor WPLabelTextColor]];
        [_headLine setFont:[UIFont fontWithName:@"HelveticaNeue" size:FONT_SIZE_MEDIUM]];
        [_headLine setText:nil];
        [_headLine setUserInteractionEnabled:NO];
        [self.contentView addSubview:_headLine];
        
        //image
        _thumbnailImageView = [[UIImageView autolayoutView] retain];
        _thumbnailImageView.backgroundColor = [UIColor clearColor];
        _thumbnailImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_thumbnailImageView];
        
        //slugLine
        _slugLine = [[UILabel autolayoutView] retain];
        [_slugLine setBackgroundColor:[UIColor clearColor]];
        [_slugLine setNumberOfLines:0];
        [_slugLine setLineBreakMode:NSLineBreakByWordWrapping];
        [_slugLine setTextColor:[UIColor WPLabelTextColor]];
        [_slugLine setFont:[UIFont fontWithName:@"Helvetica" size:FONT_SIZE_SMALL]];
        [_slugLine setText:nil];
        [_slugLine setUserInteractionEnabled:NO];
        [self.contentView addSubview:_slugLine];
        
        //dateLine
        _dateLine = [[UILabel autolayoutView] retain];
        [_dateLine setBackgroundColor:[UIColor clearColor]];
        [_dateLine setTextColor:[UIColor WPLabelTextColor]];
        [_dateLine setFont:[UIFont fontWithName:@"Helvetica" size:FONT_SIZE_SMALLER]];
        [_dateLine setText:nil];
        [self.contentView addSubview:_dateLine];
        
        //setup cache
        self.imageCache = [[[NSCache alloc] init] autorelease];
        [self.imageCache setCountLimit:24];
    }
    return self;
}

- (void)updateConstraints{
    [super updateConstraints];
    
    if (self.didSetupConstraints) {
        return;
    }
    
    // Create the views and metrics dictionaries
    [self.headLine setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
    [self.headLine setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
    [self.slugLine setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
    [self.slugLine setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
    
    NSDictionary *metrics = @{@"headHeight":@41.0, @"slugHeight":@100.0, @"dateHeight":@6.0};
    NSDictionary *views = NSDictionaryOfVariableBindings(_thumbnailImageView, _headLine, _slugLine, _dateLine);
    
    NSArray *myConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"|-[_thumbnailImageView(==45)]-[_headLine]-|" options:0 metrics:metrics views:views];
    
    [self.contentView addConstraints:myConstraint];

    myConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(8)-[_headLine(21@500)]-(4)-[_dateLine(dateHeight)]-(4)-[_slugLine(21@500)]-(8)-|" options:NSLayoutFormatAlignAllLeft | NSLayoutFormatAlignAllRight metrics:metrics views:views];
    
    [self.contentView addConstraints:myConstraint];
    /*
    myConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[_slugLine(<=slugHeight@252)]" options:0 metrics:metrics views:views];
    
    [self.contentView addConstraints:myConstraint];
    
    myConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[_slugLine(>=slugHeight@751)]" options:0 metrics:metrics views:views];
    
    [self.contentView addConstraints:myConstraint];
    
    myConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[_headLine(<=headHeight@251)]" options:0 metrics:metrics views:views];
    
    [self.contentView addConstraints:myConstraint];
    
    myConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[_headLine(>=headHeight@750)]" options:0 metrics:metrics views:views];
 
    [self.contentView addConstraints:myConstraint];
    */
    myConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(==12@750)-[_thumbnailImageView(==30)]" options:0 metrics:metrics views:views];
    
    [self.contentView addConstraints:myConstraint];
    
    self.didSetupConstraints = YES;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    // Make sure the contentView does a layout pass here so that its subviews have their frames set, which we
    // need to use to set the preferredMaxLayoutWidth below.
    [self.contentView setNeedsLayout];
    [self.contentView layoutIfNeeded];
    
    // Set the preferredMaxLayoutWidth of the mutli-line bodyLabel based on the evaluated width of the label's frame,
    // as this will allow the text to wrap correctly, and as a result allow the label to take on the correct height.
    self.headLine.preferredMaxLayoutWidth = CGRectGetWidth(self.headLine.frame);
    self.slugLine.preferredMaxLayoutWidth = CGRectGetWidth(self.slugLine.frame);
}

-(void)updateDate:(NSString*)dateString
{
    NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:SSZZZZ"];
    NSDate *theDate = [formatter dateFromString:dateString];
    [formatter setDateFormat:@"dd/MM/yyyy HH:mm"];
    _dateLine.text  = [formatter stringFromDate:theDate];
}

-(void)updateImageURL:(NSString *)thumbnailURLString WithIdentifier:(NSString *)identifier
{
    /*
    UIImage *cachedImage = [_imageCache objectForKey:identifier];
    
    //try cache image first
    if (cachedImage) {
        __block UIImageView *wself = self.thumbnailImageView;
        dispatch_main_sync_safe(^{
            if (!wself) return;
            wself.image = cachedImage;
            [wself setNeedsLayout];
        });
    } else {
        //otherwise load remotely
        if (thumbnailURLString && ![thumbnailURLString isEqual:[NSNull null]]){
            NSURL * imageURL =  [NSURL URLWithString:thumbnailURLString];
            NSData *imageData = [[[NSData alloc] initWithData:[NSData dataWithContentsOfURL:imageURL]] autorelease];
            UIImage *thumbnailImage = [[[UIImage alloc] initWithData:imageData] autorelease];
            
            if (thumbnailImage) {
                __block UIImageView *wself = self.thumbnailImageView;
                dispatch_main_sync_safe(^{
                    if (!wself) return;
                    wself.image = thumbnailImage;
                    [wself setNeedsLayout];
                });
                
                //cache image
                [self.imageCache setObject:thumbnailImage forKey:identifier];
            }
        }else{
            NSLog(@"Image NULL");
        }
    }*/
}

@end
