//
//  WPListTableViewCell.h
//  westpactest
//
//  Created by Johnny ddb on 2/07/2014.
//  Copyright (c) 2014 westpac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WPListTableViewCell : UITableViewCell

@property(nonatomic, retain) UIImageView *thumbnailImageView;
@property(nonatomic, retain) UILabel *headLine;
@property(nonatomic, retain) UILabel *dateLine;
@property(nonatomic, retain) UILabel *slugLine;

-(void)updateDate:(NSString*)dateString;
-(void)updateImageURL:(NSString *)thumbnailURLString WithIdentifier:(NSString *)identifier;

@end
