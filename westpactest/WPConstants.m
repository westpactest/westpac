//
//  WPConstants.m
//  westpactest
//
//  Created by Johnny ddb on 2/07/2014.
//  Copyright (c) 2014 westpac. All rights reserved.
//

#import "WPConstants.h"

NSString * const APP_NAME = @"News";
NSString * const DATA_URL = @"http://mobilatr.mob.f2.com.au/services/views/9.json";

float const FONT_SIZE_SMALLER = 8.0f;
float const FONT_SIZE_SMALL = 10.0f;
float const FONT_SIZE_MEDIUM = 12.0f;
float const FONT_SIZE_LARGER = 14.0f;